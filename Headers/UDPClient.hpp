#ifndef UDPENCRYPTEDCLIENT_UDPCLIENT_HPP
#define UDPENCRYPTEDCLIENT_UDPCLIENT_HPP

#include <netinet/in.h>
#include <string>
#include <arpa/inet.h>

class UDPClient {
private:
    int remoteServerPort;
    std::string remoteServerAddress;
    int clientSocket;
    struct sockaddr_in serverAddrData{};
    socklen_t addr_size;

    void InitializeConnection() {
        clientSocket = socket(PF_INET, SOCK_DGRAM, 0);

        serverAddrData.sin_family = AF_INET;
        serverAddrData.sin_port = htons(remoteServerPort);
        serverAddrData.sin_addr.s_addr = inet_addr(remoteServerAddress.c_str());
        memset(serverAddrData.sin_zero, '\0', sizeof serverAddrData.sin_zero);

        addr_size = sizeof(serverAddrData);
    }

public:

    UDPClient(int port, std::string address) {
        remoteServerPort = port;
        remoteServerAddress = address;

        InitializeConnection();
    }

    bool SentDatagramToServerSuccessfully(char* buffer, int bufferByteCount) {
        bool successfulSend = true;
        if (sendto(clientSocket, buffer, bufferByteCount, 0, (struct sockaddr*)&serverAddrData, addr_size) < 0) {
            successfulSend = false;
        }
        return successfulSend;
    }

    void ReceiveDatagramFromServer(int bufferLimit, char* buffer, int& receivedByteCount) {
        receivedByteCount = recvfrom(clientSocket, buffer, bufferLimit, 0, nullptr, nullptr);
    }

};

#endif //UDPENCRYPTEDCLIENT_UDPCLIENT_HPP
